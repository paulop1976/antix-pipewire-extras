��          L      |       �      �   |   �   t   /  "   �     �    �     N  �   W  t   �  *   \  
   �                                         PipeWire PipeWire audio server IS NOT currently on the antiX startup and WILL NOT start automatically the next time you restart antiX PipeWire audio server IS currently on the antiX startup and WILL start automatically the next time you restart antiX PipeWire seems to not be installed Reboot Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-08-04 13:02+0000
Last-Translator: Vanhoorne Michael, 2023
Language-Team: Dutch (Belgium) (https://app.transifex.com/anticapitalista/teams/10162/nl_BE/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: nl_BE
Plural-Forms: nplurals=2; plural=(n != 1);
 PipeWire PipeWire-audioserver IS momenteel NIET bezig met het opstarten van antiX en start NIET automatisch de volgende keer dat u antiX opnieuw opstart PipeWire audio server IS currently on the antiX startup and WILL start automatically the next time you restart antiX PipeWire lijkt niet te zijn geïnstalleerd Herstarten 