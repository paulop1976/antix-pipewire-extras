��          L      |       �      �   |   �   t   /  "   �     �  �  �     Y    b    x  P   �     �                                         PipeWire PipeWire audio server IS NOT currently on the antiX startup and WILL NOT start automatically the next time you restart antiX PipeWire audio server IS currently on the antiX startup and WILL start automatically the next time you restart antiX PipeWire seems to not be installed Reboot Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-08-04 13:02+0000
Last-Translator: anticapitalista <anticapitalista@riseup.net>, 2023
Language-Team: Greek (https://app.transifex.com/anticapitalista/teams/10162/el/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: el
Plural-Forms: nplurals=2; plural=(n != 1);
 PipeWire Ο διακομιστής ήχου PipeWire ΔΕΝ βρίσκεται επί του παρόντος στην εκκίνηση του antiX και ΔΕΝ ΘΑ ξεκινήσει αυτόματα την επόμενη φορά που θα επανεκκινήσετε το antiX Ο διακομιστής ήχου PipeWire βρίσκεται επί του παρόντος στην εκκίνηση του antiX και ΘΑ ξεκινήσει αυτόματα την επόμενη φορά που θα επανεκκινήσετε το antiX Το PipeWire φαίνεται να μην είναι εγκατεστημένο Επανεκκίνηση 