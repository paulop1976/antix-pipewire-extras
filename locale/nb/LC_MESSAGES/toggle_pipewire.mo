��          L      |       �      �   |   �   t   /  "   �     �  �  �     Y  z   b  p   �     N     l                                         PipeWire PipeWire audio server IS NOT currently on the antiX startup and WILL NOT start automatically the next time you restart antiX PipeWire audio server IS currently on the antiX startup and WILL start automatically the next time you restart antiX PipeWire seems to not be installed Reboot Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-08-04 13:02+0000
Last-Translator: heskjestad <cato@heskjestad.xyz>, 2023
Language-Team: Norwegian Bokmål (https://app.transifex.com/anticapitalista/teams/10162/nb/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: nb
Plural-Forms: nplurals=2; plural=(n != 1);
 PipeWire PipeWire lydtjener er IKKE aktivert i oppstarten av antiX, og vil ikke starte automatisk neste gang antiX startes på nytt PipeWire lydtjener er AKTIVERT i oppstarten av antiX, og vil starte automatisk neste gang antiX startes på nytt Fant ikke noen PipeWire-filer Omstart 